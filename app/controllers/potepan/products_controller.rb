class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.limit(Constants::MAX_RELATED_PRODUCTS_COUNT)
  end
end
