require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  subject { full_title(page_title) }

  context "page title whitespace" do
    let(:page_title) { " " }

    it { is_expected.to eq "BIG BAG" }
  end

  context "page title not empty" do
    let(:page_title) { "sample" }

    it { is_expected.to eq "sample | BIG BAG" }
  end

  context "page title nil" do
    let(:page_title) { nil }

    it { is_expected.to eq "BIG BAG" }
  end

  context "page title empty" do
    let(:page_title) { "" }

    it { is_expected.to eq "BIG BAG" }
  end
end
