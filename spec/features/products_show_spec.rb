require 'rails_helper'

RSpec.feature "ProductsShows", type: :feature do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon) { create(:taxon) }
  let!(:related_products) { create_list(:product, Constants::MAX_RELATED_PRODUCTS_COUNT + 1, taxons: [taxon]) }
  let!(:not_related_product) { create(:product, name: 'not_related_product') }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "user go to product_show" do
    expect(page).to have_title product.name
    expect(page).to have_css "div.header"
    expect(page).to have_css "div#footer"

    within ".header" do
      home_link = find(".navbar-brand")
      expect(home_link[:href]).to eq potepan_index_path
      expect(page).to have_link 'Home', href: potepan_index_path
    end

    within '.pageHeader' do
      expect(page).to have_link 'Home', href: potepan_index_path
      expect(page).to have_selector "li", text: "Home"
      expect(page).to have_selector "li", text: product.name
      expect(page).to have_selector "h2", text: product.name
    end

    within '.mainContent' do
      expect(page).to have_selector "h2", text: product.name
      expect(page).to have_selector "h3", text: product.display_amount
      expect(page).to have_selector "p", text: product.description
    end

    within 'div.productsContent' do
      expect(page).to have_selector 'h4', text: '関連商品'
      related_products.first(Constants::MAX_RELATED_PRODUCTS_COUNT).each do |related_product|
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_amount
      end
      expect(page).not_to have_selector 'h5', text: related_products.last
      expect(page).not_to have_content not_related_product.name
      expect(page).not_to have_content product.name
      click_on related_products.first.name
    end

    expect(current_path).to eq potepan_product_path(related_products.first.id)
  end
end
