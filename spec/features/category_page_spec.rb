require 'rails_helper'

RSpec.feature "CategoryPages", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon1) { create(:taxon, name: 'sample1', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:taxon2) { create(:taxon, name: 'sample2', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product1) { create(:product, name: 'test1', taxons: [taxon1]) }
  let!(:product2) { create(:product, name: 'test2', taxons: [taxon2]) }

  describe "user go to category_page" do
    before do
      visit potepan_category_path(taxon1.id)
    end

    scenario "require display" do
      expect(page).to have_title taxon1.name
      expect(page).to have_css "div.header"
      expect(page).to have_css "div#footer"
      expect(page).to have_css "div.partnersLogo"

      within ".header" do
        home_link = find(".navbar-brand")
        expect(home_link[:href]).to eq potepan_index_path
        expect(page).to have_link 'Home', href: potepan_index_path
      end

      within '.pageHeader' do
        expect(page).to have_link 'Home', href: potepan_index_path
        expect(page).to have_selector "li", text: "Home"
        expect(page).to have_selector "li", text: taxon1.name
        expect(page).to have_selector "h2", text: taxon1.name
      end

      within '.side-nav' do
        expect(page).to have_content taxonomy.name
        click_on taxonomy.name
        expect(page).to have_content taxon1.name
        expect(page).to have_content taxon1.all_products.count
      end

      within '.productBox' do
        expect(page).not_to have_content product2.name
      end

      click_on taxon1.name
      within '.productCaption' do
        expect(page).to have_content product1.name
      end
      click_on product1.name
      expect(current_path).to eq potepan_product_path(product1.id)
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon1.id)
    end
  end
end
