require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /potepan/products" do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }
    let!(:related_products) { create_list(:product, Constants::MAX_RELATED_PRODUCTS_COUNT, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "require response 200" do
      expect(response).to have_http_status(200)
    end

    it "require rendering products_show" do
      expect(response).to render_template :show
    end

    it "return product" do
      expect(assigns(:product)).to eq product
    end

    it "return related_products" do
      expect(assigns(:related_products)).to eq related_products
    end
  end
end
