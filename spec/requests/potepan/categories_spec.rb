require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET potepan/categry" do
    let(:taxonomies) { create_list(:taxonomy, 2) }
    let(:taxon) { create(:taxon, name: 'sample', taxonomy: taxonomies[0], parent_id: taxonomies[0].root.id) }
    let!(:products) { create_list(:product, 2, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "require response 200" do
      expect(response).to have_http_status(200)
    end

    it "require rendering potepan_category" do
      expect(response).to render_template :show
    end

    it "require instance variables in response" do
      expect(assigns(:taxonomies)).to eq taxonomies
      expect(assigns(:taxon)).to eq taxon
      expect(assigns(:products)).to eq products
    end
  end
end
