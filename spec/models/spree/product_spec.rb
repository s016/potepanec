require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:product1) { create(:product, taxons: [taxon1]) }
  let(:product2) { create(:product, taxons: [taxon2]) }
  let(:taxon1) { create(:taxon) }
  let(:taxon2) { create(:taxon) }
  let(:related_products) { create_list(:product, Constants::MAX_RELATED_PRODUCTS_COUNT, taxons: [taxon1]) }

  describe 'related_products' do
    it 'includes pruducts which belong to the some taxon' do
      expect(product1.related_products).to match_array related_products
    end

    it 'do not include products which belong to the some taxon2' do
      expect(product1.related_products).not_to include product2
    end

    it 'do not include product1' do
      expect(product1.related_products).not_to include product1
    end
  end
end
